// Amplia el tipo Response con un parsedBody opcional
// TODO: Echarle el guante al tipo response y ver qué tiene dentro
interface HttpResponse<T> extends Response {
  parsedBody?: T;
};

// http es una promesa que recibe una request y devuelve una respuesta
// La request se va a componer de dos argumentos: la ruta del back, un cuerpo (opcional) y unos argumentos
// Los argumentos incluyen el método http a utilizar y los headers
// Sin ver bien la forma de Request y Response, esto es todo un poco magia
// TODO: recomprobar RequestInfo
const http = async<T>(request: RequestInfo): Promise<HttpResponse<T>> => {
  const response: HttpResponse<T> = await fetch(request);
  console.log(response);

  try {
    console.log(`executing http function`);
    (response.parsedBody as unknown) = await response.json();
  } catch (ex) {
    throw new Error(ex);
  }

  if (!response.ok) {
    throw new Error(response.statusText);
  }
  return response;
};

// Una vez aceptada la función http, crear funciones que la utilicen para get/post etc es cuestión sencillas
export const get = async <T>(
  path: string,
  args: RequestInit = {
    method: "get",
    headers: {
      "Content-Type": "application/json",
    }
  }
): Promise<HttpResponse<T>> => await http<T>(new Request(path, args));

export const post = async <T>(
  path: string,
  body: unknown,
  args: RequestInit = {
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  }
): Promise<HttpResponse<T>> => await http<T>(new Request(path, args));

export async function put<T>(
  path: string,
  body: unknown,
  args: RequestInit = { method: "put", body: JSON.stringify(body) }
): Promise<HttpResponse<T>> {
  return await http<T>(new Request(path, args));
}
