import React from 'react'
import * as Molecules from '~/components/Molecules';
import {Switch, Route} from 'react-router-dom'
import { useGlobalTheme } from "~/shared/context";
import "./Main.css";

const Main:React.FC = () =>{
  const { theme } = useGlobalTheme();
  return(
    <React.Fragment>
    <section className={`game_screen ${theme}`}>
          <Switch>
            <Route path="/play"><Molecules.Board /></Route>
            <Route path="/history"><Molecules.HistoryViewer/></Route>
            <Route path="/" exact><Molecules.Home/></Route>
          </Switch>
          </section>
    </React.Fragment>
  )

}

export default Main;
