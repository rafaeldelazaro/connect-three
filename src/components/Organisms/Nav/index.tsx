import React from "react";
import {Link} from "react-router-dom"
import * as Atoms from "~/components/Atoms";
import { useGlobalTheme } from "../../../shared/context";

const Nav:React.FC = () => {
  const { setTheme } = useGlobalTheme();
// TODO tipar bien el evento
  const setThemeFunc = (event: any) => {
    if (setTheme) {
      setTheme(event.target.value);
    }
  };

  return (
    <React.Fragment>
      <Atoms.Selector options={["dark", "light"]} action={setThemeFunc} />
      <nav>
        <Link to="/">Home</Link>
        <Link to="/play">Play now</Link>
        <Link to= "/history"> Previous games</Link>
      </nav>
    </React.Fragment>
  );
};

export default Nav;
