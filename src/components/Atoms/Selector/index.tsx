import React from "react";

interface SelectorProps {
  options: number | Array<string>;
  action: React.ChangeEventHandler<HTMLSelectElement>;
}

const Selector: React.FC<SelectorProps> = ({
  options,
  action,
}: SelectorProps) => {
  let numArray = [];
  if (typeof options === "number") {
    for (let i = 1; i <= options; i++) {
      numArray.push(i.toString());
    }
    options = numArray;
  }

  return (
    <React.Fragment>
      <select name="selector" id="selector" onChange={action}>
        {options.map((option, index) => (
          <option key={index} value={option}>
            {option}
          </option>
        ))}
      </select>
    </React.Fragment>
  );
};

export default Selector;
