import React from "react";
import "./Tile.css";

interface TileProps {
  rank: number;
  file: number;
  fillTile: Function;
  tile: string;
}

const Tile: React.FC<TileProps> = ({
  rank,
  file,
  fillTile,
  tile,
}: TileProps) => {
  return (
    <React.Fragment>
      <button className="tile" onClick={() => fillTile(rank, file)}>
        {tile}
      </button>
    </React.Fragment>
  );
};

export default Tile;
