import React from "react";

interface PromptProps {
  message: string;
  action: Function;
  btn_text: string;
}

//Darle estilo de mensaje flotante. Hacer que el mensaje del botón sea genérico para que el componente sea reutilizable
const Prompt: React.FC<PromptProps> = ({
  message,
  action,
  btn_text,
}: PromptProps) => {
  return (
    <React.Fragment>
      <div>{message}</div>
      <button onClick={() => action()}>{btn_text}</button>
    </React.Fragment>
  );
};

export default Prompt;
