import React, { useState, useEffect } from "react";

import * as Atoms from "~/components/Atoms";

import * as HttpService from "~/services/http";
import { Game } from "~/shared/types";
import { BACKENDULR } from "~/shared/config";

interface Layout {
  layout: string[][];
  activePlayer: number;
  showWin: [boolean, string];
}

interface sizeState {
  size: number;
}

// TODO: mejorar el tipado
// TODO: para librarse de aviso de lintado, actualizar el tablero a través de estado
const Board: React.FC = () => {
  // Estados, variables, constantes, efectos y selectores
  const [sizeState, setSize] = useState<sizeState>({ size: 5 });
  let { size } = sizeState;

  const initialLayout = Array.from({ length: size }, () =>
    Array(size).fill(" ")
  );

  let newLayout;

  const [board, setBoard] = useState<Layout>({
    activePlayer: 0,
    layout: initialLayout,
    showWin: [false, " "],
  });

  let { layout, activePlayer, showWin } = board;



  useEffect(() => {
    clearBoard(size);
  }, [size]);

  // Funciones del back

  const saveGame = async (): Promise<void> => {
    await HttpService.post<Game>(`${BACKENDULR}/games`, {
      id: Math.floor(Math.random()*10000),
      winner: showWin[1].replace("wins", ""),
      board_size: size,
    });
  };



  // Funciones

  const newGame = (size: number) => {
    saveGame();
    clearBoard(size);
  };

  const changeSize = (event: any) => {
    setSize({ ...sizeState, size: Number(event.target.value) });
  };

  const playerTokens = ["X", "O"];

  const fillTile = (rank: number, file: number) => {
    if (!(layout[rank][file] === " ") || showWin[0]) return;
    layout[rank][file] = playerTokens[activePlayer];
    const promptInstructions: [boolean, string] = decideWinner(rank, file)
      ? [true, `Player ${activePlayer + 1} wins`]
      : !layout.join("").includes(" ")
      ? [true, "A Draw"]
      : [false, " "];
    setBoard({ ...board, layout, showWin: promptInstructions });
    !promptInstructions[0] && passTurn();
  };

  const passTurn = () => {
    activePlayer === 0 ? (activePlayer = 1) : (activePlayer = 0);
    setBoard({ ...board, activePlayer });
  };

  const clearBoard = (size: number) => {
    newLayout = Array(size)
      .fill("")
      .map(() => Array(size).fill(" "));
    setBoard({
      ...board,
      activePlayer: 0,
      showWin: [false, " "],
      layout: newLayout ? newLayout : initialLayout,
    });
  };

  const decideWinner = (rank: number, file: number) => {
    //Creamos la string que vamos a buscar
    const targetValue: string =
      playerTokens[activePlayer] +
      playerTokens[activePlayer] +
      playerTokens[activePlayer];

    //La comprobación horizontal es inmediata por ser el primer nivel de la matriz
    const horizontalCheck: boolean = layout[rank]
      .join("")
      .includes(targetValue);

    // Para la comprobación vertical basta con un sencillo mapeo de la columna
    const verticalCheck: boolean = layout
      .map((rank) => rank[file])
      .join("")
      .includes(targetValue);

    // Para que la comprobación diagonal funcione en cualquier tamaño
    // Primero calculamos la distancia entre la diagonal actual y una de las diagonales principales
    const directDiagonalShift = Math.max(rank - file, file - rank);

    // Luego calculamos la primera casilla de la diagonal
    const directDiagonalOrigin = [
      rank > file ? directDiagonalShift : 0,
      rank > file ? 0 : directDiagonalShift,
    ];
    // Utilizamos el origen de la diagonal para mapear la diagonal
    const directDiagonal = [];
    for (let i = 0; i < size - directDiagonalShift; i++) {
      directDiagonal.push(
        layout[directDiagonalOrigin[0] + i][directDiagonalOrigin[1] + i]
      );
    }
    // Por fin, comprobamos si la diagonal tiene el valor que buscamos
    const directDiagonalCheck: boolean = directDiagonal
      .join("")
      .includes(targetValue);

    //Los cálculos para las diagonales inversas son algo diferentes, pero el proceso es el mismo
    const inverseDiagonalShift = Math.abs(size - 1 - rank - file);

    const inverseDiagonalOrigin = [
      rank + file < size ? 0 : inverseDiagonalShift,
      rank + file < size ? size - 1 - inverseDiagonalShift : size - 1,
    ];

    const inverseDiagonal = [];
    for (let i = 0; i < size - inverseDiagonalShift; i++) {
      inverseDiagonal.push(
        layout[inverseDiagonalOrigin[0] + i][inverseDiagonalOrigin[1] - i]
      );
    }
    const inverseDiagonalCheck = inverseDiagonal.join("").includes(targetValue);

    // Comprobamos si alguna de las cuatro condiciones se cumplen
    return (
      horizontalCheck ||
      verticalCheck ||
      directDiagonalCheck ||
      inverseDiagonalCheck
    );
  };

  return (
    <React.Fragment>
      <section >
        {showWin[0] && (
          <Atoms.Prompt
            message={`${showWin[1]}`}
            action={() => newGame(size)}
            btn_text="New game"
          />
        )}

        <Atoms.Selector options={6} action={changeSize} />

        <h1>{`Turn of player ${activePlayer + 1}`}</h1>
        <article className="board">
          {layout.map((rank, rankId) => (
            <div className="file" key={rankId}>
              {rank.map((tile, fileId) => (
                <Atoms.Tile
                  key={fileId}
                  rank={rankId}
                  file={fileId}
                  tile={tile}
                  fillTile={fillTile}
                />
              ))}
            </div>
          ))}
        </article>
      </section>
    </React.Fragment>
  );
};

export default Board;
