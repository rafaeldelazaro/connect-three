import React, { useEffect, useState, Dispatch } from "react";
import { useDispatch } from "react-redux";
import * as HttpService from "~/services/http";
import { BACKENDULR } from "~/shared/config";
import { HistoryActions } from "~/redux/actions/historyActions";
import { Game } from "~/shared/types";

const HistoryViewer: React.FC = () => {
  const [history, setHistory] = useState<Game[]>();
  const historyDispatch = useDispatch<Dispatch<HistoryActions>>();

  const getFullHistory = async (): Promise<void> => {
    const res = await HttpService.get<Game[]>(`${BACKENDULR}/games`);
    const { parsedBody } = res;
    console.log(`Sale del get`);
    setHistory(parsedBody);
    historyDispatch({ type: "GET_HISTORY", payload: parsedBody });
  };

  useEffect(() => {
    getFullHistory();
  }, []);

  return (
    <React.Fragment>
      <table className="table">
        <thead>
          <tr>
            <th style={{ width: "150px" }}>Number</th>
            <th style={{ width: "300px" }}>Winner</th>
            <th style={{ width: "300px" }}>Board Size</th>
          </tr>
        </thead>
        <tbody>
          {history &&
            history.map((game: Game, idx: number) => (
              <tr key={idx}>
                <td>{game.id}</td>
                <td>{game.winner}</td>
                <td>{game.board_size}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </React.Fragment>
  );
};

export default HistoryViewer;
