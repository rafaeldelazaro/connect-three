export interface Game {
  id: number;
  winner: string;
  board_size: number;
}
