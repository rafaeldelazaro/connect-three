// Deprecated file

// La estructura básica de los datos que va a tener el reductor. Realmente depende muchísimo del uso que se le vaya a dar.
export enum ThemeTypes {
  light = "light",
  dark = "dark",
}
