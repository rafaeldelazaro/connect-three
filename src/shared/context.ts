import { createContext, useContext } from "react";

// Los tipos de temas
export enum Themes {
  light = "light",
  dark = "dark",
}

// el tipo global theme que le vamos a pasar al context para manipularlo, tiene estructura de useState
type GlobalTheme = {
  theme?: Themes;
  setTheme?: (theme: Themes) => void;
};

//creamos el contexto
export const GlobalThemeContext = createContext<GlobalTheme>({});

//para acceder
export const useGlobalTheme = (): GlobalTheme => useContext(GlobalThemeContext);
