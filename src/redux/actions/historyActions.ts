import { Game } from "~/shared/types";

export interface ISetHistoryActions {
  readonly type: "GET_HISTORY";
  payload?: Game[];
}

export type HistoryActions = ISetHistoryActions;
