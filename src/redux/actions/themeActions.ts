// Deprecated file
import { ThemeTypes } from "~/shared/constants";

//Estructura que va a tener cualquier solicitud de cambio de lo almacenado
export interface ISetThemeActions {
  readonly type: "SET_THEME";
  payload?: ThemeTypes;
}

export type ThemeActions = ISetThemeActions;
