import { HistoryActions } from "../actions/historyActions";

import { Game } from "~/shared/types";

// Convertimos la estructura de datos en un tipo para asegurarnos de que el reducer no recibe cosas raras
export type HistoryState = {
  history: Game[];
};

const initialState: HistoryState = {
  history: [{ id: 0, winner: `Player 0`, board_size: 0 }],
};

// Coge un estado y una acción, y devuelve ese estado modificado en función de la acción que sea
const HistoryReducer = (
  state: HistoryState,
  action: HistoryActions
): HistoryState => {
  switch (action.type) {
    case "GET_HISTORY":
      return {
        ...state,
        history: action.payload ? action.payload : initialState.history,
      };
    default:
      return state;
  }
};

export default HistoryReducer;
