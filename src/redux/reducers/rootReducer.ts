import HistoryReducer from "./historyReducer";

// Junta todas las posibles funciones reducer estructuradas para poder dárselas a la store
const rootReducer = (state: any, action: any) => HistoryReducer(state, action);

// Para asegurarnos de que los selectores reciben un dato con la estructura fijada por el reductor (???)
export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
