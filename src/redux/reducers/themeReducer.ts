// Deprecated file

import { ThemeActions } from "../actions/themeActions";

import { ThemeTypes } from "~/shared/constants";

// Convertimos la estructura de datos en un tipo para asegurarnos de que el reducer no recibe cosas raras
export type ThemeState = {
  theme: ThemeTypes;
};
// Para inicializar el estado
const initialTheme: ThemeState = {
  theme: ThemeTypes.dark,
};

// Coge un estado y una acción, y devuelve ese estado modificado en función de la acción que sea
const ThemeReducer = (
  state: ThemeState = initialTheme,
  action: ThemeActions
): ThemeState => {
  switch (action.type) {
    case "SET_THEME":
      return {
        ...state,
        theme: action.payload ? action.payload : ThemeTypes.dark,
      };
    default:
      return state;
  }
};

export default ThemeReducer;
