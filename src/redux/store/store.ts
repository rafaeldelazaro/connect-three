import { createStore } from "redux";
import { devToolsEnhancer } from "redux-devtools-extension";

import rootReducer from "../reducers/rootReducer";
import { HistoryState } from "../reducers/historyReducer";
import { HistoryActions } from "../actions/historyActions";

// Creación del lugar donde se va a almacenar la información
const store = createStore<HistoryState, HistoryActions, null, null>(
  rootReducer,
  devToolsEnhancer({})
);

export default store;
