import React, { useState } from "react";
import * as Organisms from "~/components/Organisms";
import "./App.css";
import {BrowserRouter} from "react-router-dom"
import { GlobalThemeContext, Themes } from "./shared/context";


function App() {
  const [theme, setTheme] = useState<Themes>(Themes.dark);

  return (
    <React.Fragment>
      <BrowserRouter>
        <GlobalThemeContext.Provider value={{ theme, setTheme }}>
          <Organisms.Nav />
          <Organisms.Main />
          </GlobalThemeContext.Provider>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
